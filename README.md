cnn-intl-most-popular

Description:
A standalone node.js script for updating most popular stories box on international homepage.

Requirement:
1. ssh2 (https://github.com/mscdex/ssh2)
2. Public key set up at aps8prod4.turner.com (APS server)
3. Private key path at local (ARGO server)
4. Data source at http://data.cnn.com/top-stories/international/top-stories.json

Installation:
1. Copy public key to aps8prod4.turner.com
2. Modify the private key path in line 78
3. Schedule to script to run.
4. If the json source is not updated the script will still run with old data.