'use strict';

/**
* Reads in the top-stories.json feed and transforms it into XML to populate top stories
* The previous implementation uses RTM but the server crashed, and it is now being manually updated
* TO BE DONE
* 1. json feed needs to be updated daily on data.cnn.com
* 2. Run this script to validate that the correct XML feed is being generated
* 3. Add any unit tests and error control
* 4. Once step 2 is validated, productize this script (add it to source control, setup a cron job, work with Cisco to setup monitoring, etc.
*/

var Connection = require('ssh2');

var http = require('http'),
    fs = require('fs'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser(),
    wstream = fs.createWriteStream('intl_popular_news.xml'),  // generated XML feed
    url = require('url'),
    options = {
       host: 'data.cnn.com',
       path: '/top-stories/international/top-stories.json'
    };

var callback = function(response) {
   var str = '';

   response.on('data', function (chunk) {
        str += chunk;
    });

    // write out header of XML feed
    var outputString = ''; // needed for xml validation
    outputString += '<?xml version="1.0" encoding="UTF-8"?><intl_popular_news><toptopics/><topstories>\n';
    // for top stories in JSON feed, parse each item and transform to XML
    response.on('end', function () {
        var data = JSON.parse(str),
            storyArray = null;

        if (data !== undefined && data !== null) {
                    storyArray = data.response;
            if (storyArray !== undefined && storyArray !== null) {
                storyArray.forEach(function(value) {
                    if (typeof value !== 'undefined' && value.url !== undefined && value.url !== null) {
                       var pathname = url.parse(value.url).pathname;
                       var pathname = pathname.replace("index.html", "index.xml");
                       outputString += '<url url=\"http://edition.cnn.com' + value.url + '\" name=\"' + value.headline + '\" hits=\"' + value.pageViews + '\">';
                       outputString += '<xi:include xmlns:xi=\"http://www.w3.org/2001/XInclude\" href=\"/www/aps/cnn' + pathname + '"/></url>\n';
                    } // if value
                });
             } // if storyArray
        } // if data
        // write out footer of XML feed
        outputString += '</topstories></intl_popular_news>\n';
        // validate xml
        parser.parseString(outputString, function (err, result) {
            wstream.write(outputString);
         });
        wstream.end();
      });
}

var conn = new Connection();
conn.on('ready', function() {
  console.log('Connection :: ready');
  conn.sftp(function(err, sftp) {
    if (err) throw err;
    sftp.fastPut('./intl_popular_news.xml', '/www/aps/incoming/real.time/cnn_beta/intl_popular_news.xml', {}, function(err) {
	    if (err) throw err;
        conn.end();
    });
  });
}).connect({
  host: 'aps8prod4.turner.com',
  port: 22,
  username: 'orb',
  privateKey: require('fs').readFileSync('./.ssh/id_rsa')
});

var req = http.request(options, callback);
req.on('socket', function(socket){
     socket.setTimeout(10000);
     socket.on('timeout', function(){
         socket.destroy();
         console.log('request timeout');
     })
});
req.on('error', function(e) {
    console.log(e.message);
});
req.end();
